# ics-ans-role-pva-gateway-docker

Ansible role to deploy pvagw with docker.
The external network is created with the macvlan network.

## Role Variables

```yaml
pva_gateway_docker_image: registry.esss.lu.se/ics-docker/pva-gateway
pva_gateway_docker_image_tag: latest
pva_gateway_docker_internal_net: "internal-network"
pva_gateway_docker_external_net: "external-network"
pva_gateway_docker_external_parent: eth0
pva_gateway_docker_external_subnet: "10.0.2.15/24"
pva_gateway_docker_external_gateway: "10.0.2.1"
pva_gateway_docker_external_iprange: "10.0.2.15/24"
pva_gateway_docker_external_ip: "10.0.2.42"
pva_gateway_docker_client_addrlist: "10.0.2.255"
pva_gateway_docker_statusprefix: "{{ ansible_hostname }}:GW:STS:"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-pva-gateway-docker
```

## License

BSD 2-clause
