import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_pvagw_running(host):
    with host.sudo():
        cmd = host.command("docker logs pvagw")
    assert cmd.rc == 0
    assert "Starting pvagw" in cmd.stdout


def test_pvagw_route(host):
    with host.sudo():
        cmd = host.command("docker exec pvagw ip route")
    assert cmd.rc == 0
    assert "default via 10.0.2.1 dev eth0" in cmd.stdout
